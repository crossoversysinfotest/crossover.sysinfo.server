#!/bin/bash 

DEPLOY_FOLDER="../../deploy/server"

if [ ! -d ${DEPLOY_FOLDER} ] 
then
  mkdir ${DEPLOY_FOLDER}
fi

echo "Copying files to ${DEPLOY_FOLDER}"
cp -v bin/server ${DEPLOY_FOLDER}
cp -v etc/* ${DEPLOY_FOLDER}
echo "Finished"
