/*
 * Definitions.h
 *
 *  Created on: 28 de out de 2017
 *      Author: danilo
 */

#ifndef INCLUDE_MODULES_DEFINITIONS_H_
#define INCLUDE_MODULES_DEFINITIONS_H_

/*
 * Default values for configuration file
 */
#define CONFIG_FILE_NAME "server.conf"
#define DEFAULT_PORT    5556
#define DEFAULT_TIMEOUT 1
#define DEFAULT_ADDRESS "localhost"

/*
 * Max message size in ZeroMQ
 */
#define MAX_MESSAGE_SIZE 50

/*
 * Convert CPU load to percentage
 */
#define CONVERT_TO_PERCENTAGE 1000

/*
 * When reading from thermal classes, the value is in mC
 * This constant is used to convert from mC to C
 */
#define TEMP_DIVISOR 1000

/*
 * When reading from sysinfo, must convert to KB or MB
 */
#define CONVERT_TO_KB (1024)
#define CONVERT_TO_MB (1024*1024)

#endif /* INCLUDE_MODULES_DEFINITIONS_H_ */
