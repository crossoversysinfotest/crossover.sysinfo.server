/*
 * MachineData.h
 *
 *  Created on: 28 de out de 2017
 *      Author: danilo
 */

#ifndef INCLUDE_MODULES_MACHINEDATA_H_
#define INCLUDE_MODULES_MACHINEDATA_H_

class MachineData {
public:
  MachineData();
  MachineData(int id, int mem, int cpu, int temp, int proc);
  ~MachineData();

  void set_id(int id);
  void set_mem(int mem);
  void set_cpu(int cpu);
  void set_temp(int temp);
  void set_proc(int proc);
  int get_id();
  int get_mem();
  int get_cpu();
  int get_temp();
  int get_proc();

private:
  int id_;
  int memory_free_;
  int cpu_usage_;
  int cpu_temperature_;
  int process_count_;
};

#endif /* INCLUDE_MODULES_MACHINEDATA_H_ */
