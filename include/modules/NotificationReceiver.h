/*
 * NotificationReceiver.h
 *
 *  Created on: 28 de out de 2017
 *      Author: danilo
 */

#ifndef INCLUDE_MODULES_NOTIFICATIONRECEIVER_H_
#define INCLUDE_MODULES_NOTIFICATIONRECEIVER_H_

#include <zmq.hpp>
#include <string>
#include <include/modules/MachineData.h>

class NotificationReceiver {
public:
  NotificationReceiver();
  NotificationReceiver(int port, std::string address);
  ~NotificationReceiver();
  void Receive(MachineData &info);
private:
  void Configure();
  int port_;
  std::string address_;
  zmq::context_t * context_;
  zmq::socket_t * subscriber_;

};

#endif /* INCLUDE_MODULES_NOTIFICATIONRECEIVER_H_ */
