/*
 * NotificationReceiverInterface.h
 *
 *  Created on: 28 de out de 2017
 *      Author: danilo
 */

#ifndef INCLUDE_NOTIFICATIONRECEIVERINTERFACE_H_
#define INCLUDE_NOTIFICATIONRECEIVERINTERFACE_H_
#include <string>

using namespace std;

class NotificationReceiverInterface {
public:
  virtual void recv(char * s) = 0;
};

#endif /* INCLUDE_NOTIFICATIONRECEIVERINTERFACE_H_ */
