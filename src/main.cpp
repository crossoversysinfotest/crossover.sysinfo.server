 
#include <iostream>
#include <include/modules/NotificationReceiver.h>
#include <include/modules/MachineData.h>
 
using namespace std;

int main()
{
  std::cout << "Server Application" << std::endl;
  MachineData data;
  NotificationReceiver notif;

  while(1) {
    notif.Receive(data);
  }

  return 0;
}

