/*
 * MachineData.cpp
 *
 *  Created on: 28 de out de 2017
 *      Author: danilo
 */

#include <include/modules/MachineData.h>

MachineData::MachineData() {
  id_ = 0;
  memory_free_ = 0;
  cpu_usage_ = 0;
  cpu_temperature_ = 0;
  process_count_ = 0;
}
MachineData::MachineData(int id, int mem, int cpu, int temp, int proc) {
  id_ = id;
  memory_free_ = mem;
  cpu_usage_ = cpu;
  cpu_temperature_ = temp;
  process_count_ = proc;

}

MachineData::~MachineData() {
}

void MachineData::set_id(int id) {
  id_ = id;
}

void MachineData::set_mem(int mem) {
  memory_free_ = mem;
}

void MachineData::set_cpu(int cpu) {
  cpu_usage_ = cpu;
}

void MachineData::set_temp(int temp) {
  cpu_temperature_ = temp;
}

void MachineData::set_proc(int proc) {
  process_count_ = proc;
}

int MachineData::get_id() {
  return id_;
}

int MachineData::get_mem() {
  return memory_free_;
}

int MachineData::get_cpu() {
  return cpu_usage_;
}

int MachineData::get_temp() {
  return cpu_temperature_;
}

int MachineData::get_proc() {
  return process_count_;
}
