/*
 * NotificationReceiver.cpp
 *
 *  Created on: 28 de out de 2017
 *      Author: danilo
 */

#include <include/modules/NotificationReceiver.h>
#include <include/modules/Definitions.h>
#include <iostream>
#include <sstream>

NotificationReceiver::NotificationReceiver() {
  port_ = DEFAULT_PORT;
  address_ = DEFAULT_ADDRESS;
  Configure();
}

NotificationReceiver::NotificationReceiver(int port, std::string address) {
  port_ = port;
  address_ = address;
  Configure();
}

void NotificationReceiver::Configure() {
  //  Prepare our context and subscriber
  context_ = new zmq::context_t(1);
  subscriber_ = new zmq::socket_t(*context_, ZMQ_SUB);
  char connection[50] = {0};
  sprintf(connection, "tcp://%s:%d", address_.c_str(), port_);
  std::cout << "Connection: " << connection << std::endl;
  subscriber_->connect(connection);
  const char *no_filter = "";
  subscriber_->setsockopt(ZMQ_SUBSCRIBE, no_filter, strlen (no_filter));
}

NotificationReceiver::~NotificationReceiver() {
  delete context_;
  delete subscriber_;
}

void NotificationReceiver::Receive(MachineData &info) {
  int id, mem, cpu, temp, proc;
  zmq::message_t update;
  subscriber_->recv(&update);
  std::istringstream iss(static_cast<char*>(update.data()));
  iss >> id >> mem >> cpu >> temp >> proc;
  std::cout << "Received:\n"
            << "Machine id: " << id << "\n"
            << " Memory free:   " << mem << " KB\n"
            << " Cpu usage:     " << cpu << " %\n"
            << " Cpu temprture: " << temp << " ºC\n"
            << " Process count: " << proc << "\n" << std::endl;

  info.set_id(id);
  info.set_mem(mem);
  info.set_cpu(cpu);
  info.set_temp(temp);
  info.set_proc(proc);
}
